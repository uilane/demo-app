import * as React from 'react';
import { CharactersTableView } from '../views';
import { api } from '../services';
import { useAsyncEffect, useDebounce } from '../helpers';
import { CharacterReadModel } from '../models';

export const getCharacters = async (searchQuery: string) => {
    const data = await api.getCharacters(searchQuery);
    return data.map((d: any) => new CharacterReadModel(d));
};

// tslint:disable-next-line variable-name
export const CharactersTableProxy: React.FunctionComponent = () => {
    const [data, setData] = React.useState([]);
    const [searchQuery, setSearchQuery] = React.useState('');
    const [debouncedSearchQuery] = useDebounce(searchQuery, 200);

    const fetchData = async () => {
        const data = await getCharacters(debouncedSearchQuery);
        setData(data);
    };

    useAsyncEffect(
        fetchData,
        [debouncedSearchQuery],
    );

    const onSearchQueryChange = (value: string) => setSearchQuery(value);

    return (
        <CharactersTableView
            data={data}
            searchQuery={searchQuery}
            onSearchQueryChange={onSearchQueryChange}
        />
    );
};
