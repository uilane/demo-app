import { IHash } from '../helpers';
import { CharacterWriteModel } from '../models';

export const apiGet = async (url: string): Promise<any> => {
    const response = await fetch(url);
    const json = await response.json();
    return json;
};

export const apiPost = async (url: string, writeModel: IHash<any>): Promise<any> => {
    const payload = {
        body: JSON.stringify(writeModel),
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(url, payload);
    const json = await response.json();
    return json;
};

export const api = {
    getSpecies: () =>
        apiGet(`${process.env.REACT_APP_API_BASE_URL}/species`),

    getCharacters: (searchQuery: string) => {
        const searchSuffix = searchQuery ? `?q=${searchQuery}` : '';
        return apiGet(`${process.env.REACT_APP_API_BASE_URL}/characters${searchSuffix}`);
    },

    postCharacters: (writeModel: CharacterWriteModel) =>
        apiPost(`${process.env.REACT_APP_API_BASE_URL}/characters`, writeModel),
};
