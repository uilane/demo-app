import * as React from 'react';
import './DataTable.scss';
import { IHash } from '../../helpers';

export interface IColumnProps {
    name: string;
    dataKey?: string;
    headerFormatter: (...args: any) => React.ReactNode;
}

export interface IDataTableProps {
    data: IHash<any>[];
    columns: IColumnProps[];
    className?: string;
    rowClassName?: string;
    cellClassName?: string;
    headerCellClassName?: string;
}

// tslint:disable-next-line variable-name
export const DataTable: React.FunctionComponent<IDataTableProps> =
    ({ data, columns, className, headerCellClassName, rowClassName, cellClassName }) => {
        const headerCells = columns.map((col: IColumnProps, colIdx: number) => {
            const props = {
                colIdx,
                name: col.name,
                dataKey: col.dataKey,
            };
            return (
                <th key={colIdx} className={headerCellClassName}>{ col.headerFormatter(props) }</th>
            );
        });

        const bodyRows = data.map((row: IHash<any>, rowIdx: number) => {

            const cells = columns.map((col: IColumnProps, colIdx: number) => {

                return (
                    <td key={colIdx} className={cellClassName}>
                        {col.dataKey && row[col.dataKey]}
                    </td>
                );
            });

            return (
                <tr key={rowIdx} className={rowClassName}>{ cells }</tr>
            );
        });

        return (
            <table className={className}>
                <thead>
                    <tr>
                        { headerCells }
                    </tr>
                </thead>
                <tbody>
                    { bodyRows }
                </tbody>

            </table>
        );
    };

DataTable.defaultProps = {
    className: 'DataTable',
    headerCellClassName: 'DataTable__HeaderCell',
    rowClassName: 'DataTable__Row',
    cellClassName: 'DataTable__Cell',
};
