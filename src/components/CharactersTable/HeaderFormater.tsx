import * as React from 'react';
import './HeaderFormater.scss';

export interface IHeaderFormaterProps {
    name: string;
}

// tslint:disable-next-line variable-name
export const HeaderFormater: React.FunctionComponent<IHeaderFormaterProps> =
    ({ name  }: IHeaderFormaterProps) => {
        return <span className="HeaderFormater">{ name }</span>;
    };
