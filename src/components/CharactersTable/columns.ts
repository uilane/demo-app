import { HeaderFormater } from './HeaderFormater';

export const columns = [
    {
        name: 'Id',
        dataKey: 'id',
        headerFormatter: HeaderFormater,
    },
    {
        name: 'Name',
        dataKey: 'name',
        headerFormatter: HeaderFormater,
    },
    {
        name: 'Gender',
        dataKey: 'gender',
        headerFormatter: HeaderFormater,
    },
    {
        name: 'Homeworld',
        dataKey: 'homeworld',
        headerFormatter: HeaderFormater,
    },
    {
        name: 'Actions',
        headerFormatter: HeaderFormater,
    },
];
