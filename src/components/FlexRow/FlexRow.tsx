import * as React from 'react';
import './FlexRow.scss';

export type TFlexRowProps = {
    children?: React.ReactNode,
};

// tslint:disable-next-line variable-name
export const FlexRow: React.FunctionComponent<TFlexRowProps> = (props: TFlexRowProps) => (
    <section className="FlexRow">
        { props.children }
    </section>
);
