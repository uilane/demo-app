import * as React from 'react';
import './NumberInput.scss';

export interface INumberInputProps {
    className?: string;
    onChange?: (value: any) => void;
    value?: string;
}

export const isValueLikeNumber = (value: any) => {
    const safeValue = value !== undefined ? value : '';
    return !isNaN(Number(safeValue));
};

export const safeOnChange =
    (value: any, cbk: (value: any) => void, inputEl: React.RefObject<HTMLInputElement>) => {
        const safeValue = `${value !== undefined ? value : ''}`;
        if (!isValueLikeNumber(safeValue)) return;
        (inputEl.current as HTMLInputElement).style.width = `${safeValue.length * 8}px`;

        cbk(safeValue);
    };

// tslint:disable-next-line variable-name
export const NumberInput: React.FunctionComponent<INumberInputProps> =
    ({ value, onChange, className }) => {
        const inputEl = React.useRef(null);
        if (!isValueLikeNumber(value)) {
            throw new Error('NumberInput value is not a number like string');
        }

        return (
            <input
                className={className}
                onChange={e => safeOnChange(e.target.value, onChange!, inputEl)}
                type="text"
                value={value}
                ref={inputEl}
            />
        );
    };

NumberInput.defaultProps = {
    className: 'NumberInput',
    value: '',
    onChange: () => undefined,
};
