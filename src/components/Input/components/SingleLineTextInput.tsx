import * as React from 'react';

export interface ISingleLineTextInputProps {
    className?: string;
    onChange?: (value: string) => void;
    value?: string;
    placeholder?: string;
}

// tslint:disable-next-line variable-name
export const SingleLineTextInput: React.FunctionComponent<ISingleLineTextInputProps> =
    ({ value, onChange, className, placeholder }) => (
        <input
            className={className}
            onChange={e => onChange!(e.target.value)}
            type="text"
            value={value}
            placeholder={placeholder}
        />
    );

SingleLineTextInput.defaultProps = {
    className: 'Input__SingleLineTextInput',
    value: '',
    onChange: () => undefined,
};
