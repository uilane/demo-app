import * as React from 'react';
import './InputWrap.scss';

export interface IInputWrapProps {
    error?: React.ReactNode;
    label?: React.ReactNode;
    children?: React.ReactNode;
    childrenClassName?: string;
    className?: string;
    errorClassName?: string;
    labelClassName?: string;
}

// tslint:disable-next-line variable-name
export const InputWrap: React.FunctionComponent<IInputWrapProps> =
    (props: IInputWrapProps) => (
            <span className={props.className}>
                <label>
                    {
                        props.label &&
                        <span className={props.labelClassName}>{ props.label }</span>
                    }
                    <br/>
                    <span className={props.childrenClassName}>{ props.children }</span>
                    <br/>
                    {
                        props.error &&
                        <span className={props.errorClassName}>{ props.error }</span>
                    }
                </label>
            </span>
    );

InputWrap.defaultProps = {
    childrenClassName: 'InputWrap_Children',
    className: 'InputWrap',
    errorClassName: 'InputWrap_Error',
    labelClassName: 'InputWrap_Label',
};
