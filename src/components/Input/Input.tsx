import * as React from 'react';
import './Input.scss';
import { InputWrap, SingleLineTextInput, NumberInput } from './components';

export enum EInputType {
    text = 'text',
    number = 'number',
    select = 'select',
    radio = 'radio',
}

export interface IInputProps {
    value?: string | number | null | undefined;
    onChange?: (value: string) => void;
    type?: EInputType;
    className?: string;
    placeholder?: string;
}

export type TInputInnerProps  = IInputProps & {
    getInputComponent: (props: IInputProps) => React.FunctionComponent<any>;
    getSafeValue: (props: IInputProps) => string;

    SingleLineTextInput: React.FunctionComponent<any>;
    NumberInput: React.FunctionComponent<any>;
};

export interface IInputVariantProps {
    value: string;
    onChange: IInputProps['onChange'];
}

export const getInputComponent = (props: TInputInnerProps):
    React.FunctionComponent<IInputVariantProps> => {
    switch (true) {

    case props.type === EInputType.text:
        return props.SingleLineTextInput;

    case props.type === EInputType.number:
        return props.NumberInput;

    case props.type === EInputType.radio:
        return props.SingleLineTextInput;

    case props.type === EInputType.select:
        return props.SingleLineTextInput;

    default: throw new Error('Can\'t select input type');
    }
};

export const getSafeValue = (props: IInputProps): string => {
    return ([undefined, null, false] as any[]).includes(props.value) ? '' : `${props.value}`;
};

// tslint:disable-next-line variable-name
export const Input: React.FunctionComponent<IInputProps> =
    (props: IInputProps) => {
        // tslint:disable-next-line variable-name
        const InputComponent = (props as TInputInnerProps).getInputComponent(props);
        const value = (props as TInputInnerProps).getSafeValue(props);

        return (
            <InputWrap>
                <InputComponent
                    className={props.className}
                    value={value}
                    onChange={props.onChange}
                    placeholder={props.placeholder}
                />
            </InputWrap>
        );
    };

Input.defaultProps = {
    NumberInput,
    SingleLineTextInput,
    getInputComponent,
    getSafeValue,
    type: EInputType.text,
    className: 'Input',
} as TInputInnerProps;
