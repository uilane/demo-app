import * as React from 'react';
import { Link } from 'react-router-dom';
import './PagesLinks.scss';

// tslint:disable-next-line variable-name
export const PagesLinks: React.FunctionComponent = () => (
    <>
        <Link to="/" className="PagesLinks__Link">List View</Link>
        <Link to="/#link1" className="PagesLinks__Link">Link1</Link>
        <Link to="/#link2" className="PagesLinks__Link">Link2</Link>
        <Link to="/#link3" className="PagesLinks__Link">Link3</Link>
    </>
);
