import * as React from 'react';
import './HamburgerButton.scss';
import { Icon, EIconNames } from '../Icon';

export type THamburgerButtonProps = {
    onClick?: (...args: any | undefined) => void,
};

// tslint:disable-next-line variable-name
export const HamburgerButton: React.FunctionComponent<THamburgerButtonProps> =
    (props: THamburgerButtonProps) => (
        <button className="HamburgerButton" onClick={props.onClick}>
            <Icon name={EIconNames.bars} />
        </button>
    );
