import * as React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Icon.scss';

export enum EIconNames {
    bars = 'bars',
}

export type TIconProps = {
    name: EIconNames,
};

// tslint:disable-next-line variable-name
export const Icon: React.FunctionComponent<TIconProps> = (props:TIconProps) => (
    <FontAwesomeIcon icon={props.name} />
);
