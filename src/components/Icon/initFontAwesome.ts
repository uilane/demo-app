import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars } from '@fortawesome/free-solid-svg-icons';

const icons = [
    faBars,
];

export const initFontAwesome = () => {
    library.add(...icons);
};
