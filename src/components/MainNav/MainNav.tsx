import * as React from 'react';
import './MainNav.scss';
import { HomeLink } from '../HomeLink';
import { HamburgerButton } from '../HamburgerButton';
import { PagesLinks } from '../PagesLinks';
import { classNames } from '../../helpers';

export type TMainNavProps = {
    children?: React.ReactNode,
};

export const defaultState = {
    isOpen: false,
};

// tslint:disable-next-line variable-name
export const MainNav: React.FunctionComponent<TMainNavProps> = (props: TMainNavProps) => {
    const [{ isOpen }, setState] = React.useState(defaultState);

    const onClick = () => setState({ isOpen: !isOpen });

    const className = classNames(
    'MainNav',
    isOpen && 'MainNav--open',
  );

    return (
      <nav className={className}>
          <HomeLink />
          <HamburgerButton onClick={onClick} />
          <div className="MainNav__Links">
            <PagesLinks />
          </div>
      </nav>
    );
};
