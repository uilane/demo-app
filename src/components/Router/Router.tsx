import * as React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { CharactersTableProxy } from '../../proxy';

// tslint:disable-next-line variable-name
export const Router: React.FunctionComponent = () => (
    <BrowserRouter>
        <Route path="/" exact component={CharactersTableProxy} />
    </BrowserRouter>
);
