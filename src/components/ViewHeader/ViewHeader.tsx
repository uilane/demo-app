import * as React from 'react';
import './ViewHeader.scss';

export type TViewHeaderProps = {
    children?: React.ReactNode,
};

// tslint:disable-next-line variable-name
export const ViewHeader: React.FunctionComponent<TViewHeaderProps> =
    (props: TViewHeaderProps) => (
        <h1 className="ViewHeader">
            { props.children }
        </h1>
    );
