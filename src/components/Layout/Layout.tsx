import * as React from 'react';
import './Layout.scss';
import { MainNav } from '../../components/MainNav';

export type TLayoutProps = {
    children?: React.ReactNode,
};

// tslint:disable-next-line variable-name
export const Layout: React.FunctionComponent<TLayoutProps> = (props: TLayoutProps) => (
    <div className="Layout">
      <MainNav />
      <main className="Layout__Container">
        { props.children }
      </main>
    </div>
);
