import * as React from 'react';
import { Link } from 'react-router-dom';
import './HomeLink.scss';

// tslint:disable-next-line variable-name
export const HomeLink: React.FunctionComponent = () => (
    <Link to="/" className="HomeLink">
        App
    </Link>
);
