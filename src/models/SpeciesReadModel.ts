import { sanitizeDataWithModel } from '../helpers';

export interface ISpeciesReadModel {
    name: string | null;
}

export class SpeciesReadModel implements ISpeciesReadModel {
    readonly name = null;

    constructor(data: any) {
        const sanitized = sanitizeDataWithModel(data, this);
        Object.assign(this, sanitized);
    }
}
