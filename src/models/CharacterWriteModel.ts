import { sanitizeDataWithModel } from '../helpers';

export interface ICharacterWriteModel {
    name: string | null;
    species: string | null;
    gender: string | null;
    homeworld: string | null;
}

export class CharacterWriteModel implements ICharacterWriteModel {
    readonly name = null;
    readonly species = null;
    readonly gender = null;
    readonly homeworld = null;

    constructor(data: any) {
        const sanitized = sanitizeDataWithModel(data, this);
        Object.assign(this, sanitized);
    }
}
