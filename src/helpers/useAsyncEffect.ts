import * as React from 'react';

/// https://github.com/DefinitelyTyped/DefinitelyTyped/issues/30551#issuecomment-461913188
export const useAsyncEffect =
    (effect: () => Promise<void | (() => void)>, dependencies?: any[]) => (
        React.useEffect(
            () => {
                const cleanupPromise = effect();
                return () => { cleanupPromise.then(cleanup => cleanup && cleanup()); };
            },
            dependencies,
        )
    );
