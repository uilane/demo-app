import { IHash } from './commonTypes';

export const sanitizeDataWithModel = (data: any, model: IHash<any>): IHash<any> => {
    const d = { ...data };

    // naive implementation, tested only for basic cases
    for (const key in d) {
        const value = d[key];
        const valueType = typeof value;
        const modelType = typeof model[key];

        if (
            !model.hasOwnProperty(key)
            || valueType === modelType
        ) {
            delete d[key];
        } else if (
            valueType === 'string'
            && !value.length
        ) {
            delete d[key];
        }
    }

    return d;
};
