import * as React from 'react';
import { Layout } from '../components/Layout';
import { ViewHeader } from '../components/ViewHeader';
import { FlexRow } from '../components/FlexRow';
import { Input } from '../components/Input';
import { DataTable } from '../components/DataTable';
import { CharacterReadModel } from '../models';
import { columns } from '../components/CharactersTable';

export interface ICharactersTableProps {
    data: CharacterReadModel[];
    searchQuery: string;
    onSearchQueryChange: (value: string) => void;
}

// tslint:disable-next-line variable-name
export const CharactersTableView: React.FunctionComponent<ICharactersTableProps> =
    ({ data, searchQuery, onSearchQueryChange }: ICharactersTableProps) => (

        <Layout>

            <ViewHeader>
                List View
            </ViewHeader>

            <FlexRow>
                <Input
                    value={searchQuery}
                    onChange={onSearchQueryChange}
                    placeholder="Search..."
                />
                <button>Add New</button>
            </FlexRow>

            <DataTable
                data={data}
                columns={columns}
            />

        </Layout>
    );
