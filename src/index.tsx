import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router } from './components/Router';
import { initFontAwesome } from './components/Icon';

initFontAwesome();
ReactDOM.render(<Router />, document.getElementById('root'));
